#include "sse.h"

#include <cassert>
#include <cmath>
#include <immintrin.h>

constexpr size_t kMSize = 4;

struct OptimizedModel {
    std::vector<__m128i> indecies;  
    std::vector<__m128> thresholds;
    std::vector<__m128> values;
};

std::shared_ptr<OptimizedModel> Optimize(const Model& model) {
    // Your code goes here.
    OptimizedModel optimized_model;
    size_t nb_m128 = static_cast<size_t>(
        std::floor(static_cast<double>(model.size()) / kMSize));

    optimized_model.indecies.reserve(nb_m128);
    optimized_model.thresholds.reserve(nb_m128);
    optimized_model.values.reserve(nb_m128);

    for (size_t i = 0; i < model.size() / kMSize; i += kMSize) {
       optimized_model.indecies.push_back(
               _mm_set_epi32(model[i+0].index,
                             model[i+1].index,
                             model[i+2].index,
                             model[i+3].index));
       optimized_model.thresholds.push_back(
               _mm_set_ps(model[i+0].threshold,
                          model[i+1].threshold,
                          model[i+2].threshold,
                          model[i+3].threshold));
       optimized_model.values.push_back(
               _mm_set_ps(model[i+0].value,
                          model[i+1].value,
                          model[i+2].value,
                          model[i+3].value));
    }

    size_t padding_size = model.size() % kMSize;
    int indecies[kMSize];
    double thresholds[kMSize];
    double values[kMSize];
    for (size_t i = 0; i < kMSize; ++i) {
        size_t model_i = (model.size() / kMSize) + i;
        indecies[i] = i < padding_size ?  model[model_i].index : 0;
        thresholds[i] = i < padding_size ? model[model_i].threshold : 0;
        values[i] = i < padding_size ? model[model_i].value : 0;
    }

    if (padding_size > 0) {
        optimized_model.indecies.push_back(
                _mm_set_epi32(indecies[0],
                              indecies[1],
                              indecies[2],
                              indecies[3]));
        optimized_model.thresholds.push_back(
                _mm_set_ps(thresholds[0],
                           thresholds[1],
                           thresholds[2],
                           thresholds[3]));
        optimized_model.values.push_back(
                _mm_set_ps(values[0],
                           values[1],
                           values[2],
                           values[3]));
    }

    return std::make_shared<OptimizedModel>(std::move(optimized_model));
}

double ApplyOptimizedModel(const OptimizedModel& model, const std::vector<float>& features) {
    assert(model.indecies.size() == model.thresholds.size());
    assert(model.thresholds.size() == model.values.size());

    __m128 result = _mm_set_ps(0.0f, 0.0f, 0.0f, 0.0f);
    for (size_t i = 0; i < model.indecies.size(); ++i) {
        __m128 f = _mm_i32gather_ps(features.data(), model.indecies[i], 1);
        __m128 cmp = _mm_cmpgt_ps(f, model.thresholds[i]);
        result = _mm_add_ps(result, _mm_mul_ps(model.values[i], cmp));
    }

    float* result_total = (float*)&result;
    return result_total[0] + result_total[1] + result_total[2] + result_total[3];
}
